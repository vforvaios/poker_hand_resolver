/* APP STATE */
const mapRanks = {
  '1': 1,
  '2': 2,
  '3': 3,
  '4': 4,
  '5': 5,
  '6': 6,
  '7': 7,
  '8': 8,
  '9': 9,
  '10': 10,
  'J': 11,
  'Q': 12,
  'K': 13
}
const cards = [
  {rank: 'Q', shape: 'hearts'},
  {rank: 'J', shape: 'hearts'},
  {rank: '10', shape: 'hearts'},
  {rank: '9', shape: 'hearts'},
  {rank: 'K', shape: 'hearts'}
]

/* VALIDATIONS */
function checkForPairs(deck) {
  let possiblePairs = []
  
  deck.forEach( function(d, index) {
    if (index < deck.length - 1 && mapRanks[d.rank] == mapRanks[deck[index+1].rank] ) {
      possiblePairs.push(d.rank)
    }
  } )
  
  return possiblePairs
}

function checkForThrees(deck) {
  let possibleThrees = []
  deck.forEach( function(d, index) {
    if (index < deck.length - 2 && mapRanks[d.rank] == mapRanks[deck[index+1].rank] && mapRanks[deck[index+2].rank] == mapRanks[deck[index+1].rank]) {
      possibleThrees.push(d.rank)
    }
  } )
  
  return possibleThrees
}

function checkForFours(deck) {
  let possibleFours = []
  
   deck.forEach( function(d, index) {
    if (index < deck.length - 3 && mapRanks[d.rank] == mapRanks[deck[index+1].rank] && mapRanks[deck[index+2].rank] == mapRanks[deck[index+1].rank] && mapRanks[deck[index+3].rank] == mapRanks[deck[index+2].rank]) {
      possibleFours.push(d.rank)
    }
  } )
  
  return possibleFours
}

function checkForStraight(deck) {
  let possibleStraight = false
  let possibleHighStraight = false
  
  possibleHighStraight = (
                            mapRanks[deck[0].rank] == 1 &&
                            mapRanks[deck[1].rank] == 10 &&
                            mapRanks[deck[2].rank] == 11 &&
                            mapRanks[deck[3].rank] == 12 &&
                            mapRanks[deck[4].rank] == 13
                         )
  
  for (i=0; i<deck.length - 1; i++) {
    possibleStraight = (mapRanks[deck[i].rank] === mapRanks[deck[i+1].rank] - 1) ? true : false
    if (!possibleStraight) { break }  
  }
  
  return possibleStraight || possibleHighStraight
}

/* ALL CARDS OF THE SAME COLOR */
function checkForFlush(deck) {
  let possibleFlush = false
  
  for (i=0; i<deck.length - 1; i++) {
    possibleFlush = (deck[i].shape === deck[i+1].shape) ? true : false
    if (!possibleFlush) { break }  
  }
  
  return possibleFlush
}

function checkForFullHouse(deck) {
  let possibleFullHouse = false
  let possibleThrees = []
  let possiblePair = []
  
  possibleThrees = checkForThrees(deck)
  if (possibleThrees.length) {
    const newDeck = [...deck].filter( d => d.rank !== possibleThrees[0] )
    possiblePair = [...checkForPairs(newDeck)]
  }
  
  possibleFullHouse = possibleThrees.length && possiblePair.length ? true : false
  
  return possibleFullHouse
}

function checkForStraightFlush(deck) {
  let possibleStraightFlush = false
  let possibleStraight = false
  let possibleFlush = false
  
  possibleStraight = checkForStraight(deck)
  possibleFlush = checkForFlush(deck)
  
  possibleStraightFlush = possibleStraight && possibleFlush
  
  return possibleStraightFlush
}

function checkForFlushRoyal(deck) {
  let possibleStraightFlush = checkForStraight(deck) && checkForFlush(deck)
  let possibleRoyal = mapRanks[deck[0].rank] === 1 && mapRanks[deck[4].rank] === 13
  
  return possibleStraightFlush && possibleRoyal
}

/* MAIN FUNCTION */
function ratingCards(hand) {
  const sortedArray = [...hand].sort((a, b) => (mapRanks[a.rank] > mapRanks[b.rank]) ? 1 : -1)
  
  if (checkForFlushRoyal(sortedArray)) {
    return "Flush Royal!"
  }
  
  if (checkForStraightFlush(sortedArray)) {
    return "Straight Flush!"
  }
  
  if (checkForFours(sortedArray).length) {
    return "Four of a kind"
  }
  
  if (checkForFullHouse(sortedArray)) {
    return "Full House!"
  }
  
  if (checkForFlush(sortedArray)) {
    return "Flush!"  
  }
  
  if (checkForStraight(sortedArray)) {
    return "Straight!"
  }
  
  if (checkForThrees(sortedArray).length) {
    return "Three of a kind"
  }
  
  if (checkForPairs(sortedArray).length === 2) {
    return "Two pair"
  }
  
  if (checkForPairs(sortedArray).length === 1) {
    return "Pair"
  }
  
  return "You hane nothing!"
}

/* OUTPUT THE RESULT OF HAND IN CONSOLE */
console.log(ratingCards(cards))
